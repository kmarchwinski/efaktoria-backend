﻿namespace eFaktoria.Model.DTO.Company
{
    public class CompanyDataDTO
    {
        public string CompanyName { get; set; }

        public string NIP { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }
    }
}
