﻿namespace eFaktoria.Model.DTO.Company
{
    public class CreateBankAccountDTO
    {
        public string AccountNumber { get; set; }

        public int BankId { get; set; }

        public bool IsMain { get; set; }
    }
}
