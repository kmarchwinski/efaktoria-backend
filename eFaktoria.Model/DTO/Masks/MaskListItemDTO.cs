﻿namespace eFaktoria.Model.DTO.Masks
{
    public class MaskListItemDTO
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public int Id { get; set; }

        public bool IsDefault { get; set; }
        public bool IsMain { get; set; }
    }
}
