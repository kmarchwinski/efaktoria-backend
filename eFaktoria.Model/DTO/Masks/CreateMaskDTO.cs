﻿using System.ComponentModel.DataAnnotations;

namespace eFaktoria.Model.DTO.Masks
{
    public class CreateMaskDTO
    {
        [Required]
        public string Mask { get; set; }

        public int StartNumber { get; set; }

        public int InvoiceTypeId { get; set; }

        public bool IsMain { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
