﻿using eFaktoria.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eFaktoria.Model.DTO
{
    public class InvoiceListItemDTO
    {
        public bool IsPaid { get; set; }

        /// <summary>
        /// Data płatności
        /// </summary>
        public DateTime PaymentDate { get; set; }

        public string Title { get; set; }

        public string ContractorName { get; set; }

        public decimal GrossValue
        {
            get
            {
                var vatRates = VATRateHelper.GetVATRates();

                decimal grossValue = 0;

                foreach (var item in PositionsNetValuesWithTax)
                {
                    var currentRate = vatRates.FirstOrDefault(x => x.Key == item.Key);

                    var grossValueItem = currentRate.Value * item.Value;

                    grossValue = item.Value + grossValueItem + grossValue;
                }

                return grossValue;
            }
        }

        /// <summary>
        /// Słownik z kwotami poszczególnych pozycji faktury z id stawki, której dotyczy
        /// </summary>
        public Dictionary<int, decimal> PositionsNetValuesWithTax { private get; set; }
    }
}
