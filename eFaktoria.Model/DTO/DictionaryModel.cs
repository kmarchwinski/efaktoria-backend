﻿namespace eFaktoria.Model.DTO
{
    public class DictionaryModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
