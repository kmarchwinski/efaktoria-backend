﻿namespace eFaktoria.Model.Attributes
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Linq;


    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class PasswordValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var password = (String)value;
            bool result = true;
            if (password != null)
            {
                result = CheckPassword(password);
            }
            return result;
        }

        private static bool CheckPassword(string password)
        {
            if (password.Any(char.IsUpper) || password.Any(char.IsDigit) || password.Any(x => !char.IsLetterOrDigit(x)))
            {
                return true;
            }

            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
                "Hasło musi się składać z małych liter i co najmniej jednej dużej litery lub cyfry lub znaku specjalnego");
        }

    }
}
