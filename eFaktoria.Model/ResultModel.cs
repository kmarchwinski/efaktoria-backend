﻿using System.Collections.Generic;
using System.Linq;

namespace eFaktoria.Model
{
    public class ResultModel<T>
    {
        public ResultModel()
        {
            Status = new StatusModel();
        }
        //obiekt lub typ modelu
        public T Data { get; set; }

        //Statusy wykonywanych poleceń
        public StatusModel Status { get; set; }

        public bool HasError => Status.Errors.Any() || Status.AccessError || Status.CriticalError || Status.NotFoundItem;

        public void AddError(string text)
        {
            Status.Errors.Add(text);
        }

        public void JoinErrors(List<string> errors)
        {
            Status.Errors.AddRange(errors);
        }

        public void SetCriticalError()
        {
            AddError("Wystąpił niespodziewany błąd podczas przetwarzania danych");
            Status.CriticalError = true;
        }

        public void SetAccessError()
        {
            AddError("Brak dostępu do danego komponentu");
            Status.AccessError = true;
        }

        public void JoinErrors<TU>(ResultModel<TU> result)
        {
            Status.Errors.AddRange(result.Status.Errors);
        }

        public void SetNotFoundError()
        {
            AddError("Nie znaleziono danego rekordu");
            Status.NotFoundItem = true;
        }

        public class StatusModel
        {
            public StatusModel()
            {
                Errors = new List<string>();
            }
            public List<string> Errors { get; set; }
            public bool CriticalError { get; set; }
            public bool AccessError { get; set; }
            public bool NotFoundItem { get; set; }

            public string JoinedErrorsHtml => string.Join("<br/>\r\n", Errors);


        }
    }
}
