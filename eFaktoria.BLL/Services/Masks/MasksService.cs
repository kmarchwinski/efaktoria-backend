﻿using eFaktoria.BLL.DB.Entities;
using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Model;
using eFaktoria.Model.DTO.Masks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;

namespace eFaktoria.BLL.Services.Masks
{
    public interface IMasksService
    {
        ResultModel<List<MaskListItemDTO>> GetMasksList(string userId, int invoiceTypeId);
        ResultModel<bool> CreateMask(string userId, CreateMaskDTO model);
        ResultModel<bool> DeleteMask(int id, string userId);
    }
    public class MasksService : GenericService, IMasksService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public MasksService(IAmbientDbContextLocator ambientDbContextLocator, IDbContextScopeFactory dbContextScopeFactory) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }


        public ResultModel<List<MaskListItemDTO>> GetMasksList(string userId, int invoiceTypeId)
        {
            var result = new ResultModel<List<MaskListItemDTO>>();
            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var list = DB.UserInvoiceMasks
                        .Where(x => x.UserId == userId && x.InvoiceTypeId == invoiceTypeId && x.IsActive).Select(
                            x => new MaskListItemDTO
                            {
                                Id = x.Id,
                                Value = x.Mask,
                                IsDefault = false,
                                IsMain = x.IsMain,
                                Name = x.Name
                            }).ToList();

                    bool hasMainMask = list.Any(x => x.IsMain);

                    var defaultMask = DB.DefaultInvoiceMasks.Where(x => x.InvoiceTypeId == invoiceTypeId).Select(
                        x => new MaskListItemDTO
                        {
                            Id = x.Id,
                            Value = x.Value,
                            IsDefault = true,
                            IsMain = !hasMainMask,
                            Name = "Domyślny wzór"
                        }).FirstOrDefault();

                    if (defaultMask == null)
                    {
                        throw new ArgumentNullException(nameof(defaultMask));
                    }

                    list.Add(defaultMask);

                    result.Data = list;
                }

            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        public ResultModel<bool> CreateMask(string userId, CreateMaskDTO model)
        {
            var result = new ResultModel<bool>();

            try
            {
                using (var transaction = _dbContextScopeFactory.CreateWithTransaction(
                    IsolationLevel.Serializable))
                {
                    DB.CheckTransaction(result, transaction, () =>
                    {
                        SetMainMask(model, userId);

                        var newMask = new UserInvoiceMask
                        {
                            IsMain = model.IsMain,
                            UserId = userId,
                            InvoiceTypeId = model.InvoiceTypeId,
                            Name = model.Name,
                            StartNumber = model.StartNumber,
                            IsActive = true,
                            Mask = model.Mask
                        };

                        DB.UserInvoiceMasks.Add(newMask);
                    });
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        public ResultModel<bool> DeleteMask(int id, string userId)
        {
            var result = new ResultModel<bool>();
            result.Data = false;
            try
            {
                using (_dbContextScopeFactory.Create())
                {
                    var mask = DB.UserInvoiceMasks.FirstOrDefault(x => x.Id == id && x.UserId.Equals(userId));

                    if (mask == null)
                    {
                        result.SetNotFoundError();
                        return result;
                    }

                    mask.IsActive = false;
                    DB.UserInvoiceMasks.AddOrUpdate(mask);
                    DB.SaveChanges();
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        private void SetMainMask(CreateMaskDTO model, string userId)
        {
            if (model.IsMain)
            {
                var oldMainMask =
                    DB.UserInvoiceMasks.FirstOrDefault(x => x.IsMain && x.UserId.Equals(userId) && x.IsActive && x.InvoiceTypeId == model.InvoiceTypeId);

                if (oldMainMask == null)
                {
                    return;
                }

                oldMainMask.IsMain = false;

                DB.UserInvoiceMasks.AddOrUpdate(oldMainMask);
            }
        }
    }
}
