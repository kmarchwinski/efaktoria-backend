﻿using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Core.Enums;
using eFaktoria.Model;
using eFaktoria.Model.DTO;
using eFaktoria.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eFaktoria.BLL.Services.Invoice
{
    public interface IGenarateInvoiceTitleService
    {
        ResultModel<string> GetTitleByMaskId(int? id, int? defaultMaskId, string userId);
        ResultModel<string> GetTitle(string userId, int invoiceTypeId);
        ResultModel<int> GetCountOfInvoice(string userId);
    }
    public class GenarateInvoiceTitleService : GenericService, IGenarateInvoiceTitleService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public GenarateInvoiceTitleService(IAmbientDbContextLocator ambientDbContextLocator, IDbContextScopeFactory dbContextScopeFactory) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<string> GetTitleByMaskId(int? id, int? defaultMaskId, string userId)
        {
            var result = new ResultModel<string>();

            try
            {
                if (id == null && defaultMaskId == null)
                {
                    throw new ArgumentNullException();
                }
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var userMask = GetMask(id, defaultMaskId, userId);

                    if (userMask == null)
                    {
                        throw new ArgumentNullException(nameof(userMask));
                    }

                    int invoiceNumber = GetInvoiceNumber(userId, userMask.Id);

                    string title = ChangeMaskToValues(userMask.Name, invoiceNumber);

                    result.Data = title;
                }

            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        private DictionaryModel GetMask(int? id, int? defaultMaskId, string userId)
        {
            DictionaryModel userMask;
            if (defaultMaskId != null)
            {
                userMask = DB.DefaultInvoiceMasks.Where(x => x.Id == defaultMaskId)
                    .Select(x => new DictionaryModel() { Name = x.Value, Id = x.InvoiceTypeId }).FirstOrDefault();
            }
            else
            {
                userMask = DB.UserInvoiceMasks
                    .Where(x => x.UserId.Equals(userId) && x.Id == id && x.IsMain && x.IsActive)
                    .Select(x => new DictionaryModel() { Name = x.Mask, Id = x.InvoiceTypeId })
                    .FirstOrDefault();
            }
            return userMask;
        }

        public ResultModel<string> GetTitle(string userId, int invoiceTypeId)
        {
            var result = new ResultModel<string>();

            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    int invoiceNumber = GetInvoiceNumber(userId, invoiceTypeId);

                    string mask = DB.UserInvoiceMasks
                        .Where(x => x.UserId.Equals(userId) && x.InvoiceTypeId == invoiceTypeId && x.IsMain && x.IsActive).Select(x => x.Mask)
                        .FirstOrDefault();

                    if (string.IsNullOrWhiteSpace(mask))
                    {
                        mask = GetDefaultMask(invoiceTypeId);
                    }

                    string title = ChangeMaskToValues(mask, invoiceNumber);

                    result.Data = title;
                }

            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        public ResultModel<int> GetCountOfInvoice(string userId)
        {
            var result = new ResultModel<int>();

            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    int invoiceNumber = GetInvoiceNumber(userId);

                    result.Data = invoiceNumber;
                }

            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        private string ChangeMaskToValues(string mask, int invoiceNumber)
        {
            var valuesToChange = GetMaskValuesToChange(mask);

            var allValues =
                DB.DictionaryPositions.Where(x => x.DictionaryId == (int)enuDictionary.InvoiceTitleSymbols).Select(x => new
                {
                    x.Id,
                    x.Name
                });

            foreach (var value in valuesToChange)
            {
                var valueWithOption = allValues.Where(x => x.Name.Equals(value)).Select(x => new DictionaryModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).FirstOrDefault();

                if (valueWithOption == null)
                {
                    throw new ArgumentOutOfRangeException(nameof(valueWithOption));
                }

                mask = ChangeSymbolToValue(mask, invoiceNumber, valueWithOption, value);
            }
            return mask;
        }

        private static string ChangeSymbolToValue(string mask, int invoiceNumber, DictionaryModel valueWithOption, string value)
        {
            switch (valueWithOption.Id)
            {
                case (int)enuMaskSymbol.MonthWithZero:
                    mask = mask.Replace(value, DateTime.Now.Month.ToString("0#"));
                    break;
                case (int)enuMaskSymbol.MonthWithoutZero:
                    mask = mask.Replace(value, DateTime.Now.Month.ToString());
                    break;
                case (int)enuMaskSymbol.NumberWithZero:
                    mask = mask.Replace(value, invoiceNumber.ToString("D2"));
                    break;
                case (int)enuMaskSymbol.NumberWithoutZero:
                    mask = mask.Replace(value, invoiceNumber.ToString());
                    break;
                case (int)enuMaskSymbol.FullYear:
                    mask = mask.Replace(value, DateTime.Now.Year.ToString());
                    break;
                case (int)enuMaskSymbol.LastTwoCharsYear:
                    mask = mask.Replace(value, DateTime.Now.Year.ToString("yy"));
                    break;
            }

            return mask;
        }

        private List<string> GetMaskValuesToChange(string mask)
        {
            var valuesToChange = new List<string>();

            for (int i = 0; i < mask.Length; i++)
            {
                if (mask[i] == '%')
                {
                    char nextChar = mask[i + 1];

                    char[] maskValues =
                    {
                        'n', 'N', 'm', 'M', 'r', 'R'
                    };

                    if (maskValues.Contains(nextChar))
                    {
                        valuesToChange.Add("%" + nextChar);
                    }
                }
            }
            return valuesToChange;
        }

        private int GetInvoiceNumber(string userId, int invoiceTypeId)
        {
            int invoicesInCurrentMonth = DB.Invoices.Count(x => x.UserId.Equals(userId) && x.InvoiceTypeId == invoiceTypeId && x.IssueDate.Month == DateTime.Now.Month);

            return invoicesInCurrentMonth + 1;
        }

        private int GetInvoiceNumber(string userId)
        {
            int invoicesInCurrentMonth = DB.Invoices.Count(x => x.UserId.Equals(userId) && x.IssueDate.Month == DateTime.Now.Month);

            return invoicesInCurrentMonth;
        }

        private string GetDefaultMask(int invoiceTypeId)
        {
            var defaultMask = DB.DefaultInvoiceMasks.Where(x => x.InvoiceTypeId == invoiceTypeId).Select(x => x.Value)
                .FirstOrDefault();

            return defaultMask;
        }
    }
}
