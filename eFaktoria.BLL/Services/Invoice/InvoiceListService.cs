﻿using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Core.Helpers;
using eFaktoria.Model;
using eFaktoria.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eFaktoria.BLL.Services.Invoice
{
    public interface IInvoiceListService
    {
        ResultModel<List<InvoiceListItemDTO>> GetInvoiceListForUser(string userId, int pageSize, int pageNumber);
    }
    public class InvoiceListService : GenericService, IInvoiceListService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public InvoiceListService(
            IAmbientDbContextLocator ambientDbContextLocator,
            IDbContextScopeFactory dbContextScopeFactory
        ) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<List<InvoiceListItemDTO>> GetInvoiceListForUser(string userId, int pageSize, int pageNumber)
        {
            var result = new ResultModel<List<InvoiceListItemDTO>>();
            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var list = DB.Invoices.Select(x =>
                    new
                    {
                        Invoice = new InvoiceListItemDTO
                        {
                            ContractorName = x.Contractor.CompanyName,
                            IsPaid = x.IsPaid,
                            PaymentDate = x.PaymentDate,
                            Title = x.Title
                        },
                        InvoicePositions = x.InvoicePositions.Where(y => y.InvoiceId == x.Id).Select(t => new { t.VatRateId, t.NetValue })
                    })
                    .AsEnumerable().Select(x =>
                        {
                            x.Invoice.PositionsNetValuesWithTax =
                                x.InvoicePositions.ToDictionary(z => z.VatRateId, z => z.NetValue);
                            return x.Invoice;
                        })
                    .Page(pageSize, pageNumber)
                    .ToList();

                    result.Data = list;
                }

            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }
    }
}
