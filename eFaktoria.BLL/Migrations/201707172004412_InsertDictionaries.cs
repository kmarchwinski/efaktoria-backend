namespace eFaktoria.BLL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InsertDictionaries : DbMigration
    {
        public override void Up()
        {
            Sql(@"
SET IDENTITY_INSERT dbo.Dictionaries ON
INSERT INTO dbo.Dictionaries (Id,Name,CreatedDate,UpdatedDate) VALUES ('1','Stawka VAT','2017-07-17','2017-07-17');
INSERT INTO dbo.Dictionaries (Id,Name,CreatedDate,UpdatedDate) VALUES ('2','Metoda p�atno�ci','2017-07-17','2017-07-17');
INSERT INTO dbo.Dictionaries (Id,Name,CreatedDate,UpdatedDate) VALUES ('3','Typ faktury','2017-07-17','2017-07-17');
INSERT INTO dbo.Dictionaries (Id,Name,CreatedDate,UpdatedDate) VALUES ('4','Banki','2017-07-17','2017-07-17');
INSERT INTO dbo.Dictionaries (Id,Name,CreatedDate,UpdatedDate) VALUES ('5','Znaki w tytule faktury','2017-07-21','2017-07-21');
SET IDENTITY_INSERT dbo.Dictionaries OFF

SET IDENTITY_INSERT dbo.DictionaryPositions ON
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('100','23%','1','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('101','8%','1','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('102','5%','1','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('103','0%','1','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('104','zw','1','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('105','oo','1','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('200','Przelew','2','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('201','Got�wka','2','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('300','Faktura VAT','3','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('301','Faktura proforma','3','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('302','Faktura mar�a','3','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('400','PKO BP','4','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('401','mBank','4','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('402','BG�','4','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('403','Idea Bank','4','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('404','Lion\'s Bank','4','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('405','Pekao SA','4','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('500','%N','5','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('501','%n','5','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('502','%M','5','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('503','%m','5','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('504','%R','5','2017-07-17','2017-07-17');
INSERT INTO dbo.DictionaryPositions (Id,Name,DictionaryId,CreatedDate,UpdatedDate) VALUES ('505','%r','5','2017-07-17','2017-07-17');

SET IDENTITY_INSERT dbo.DictionaryPositions OFF
");
        }

        public override void Down()
        {
        }
    }
}
