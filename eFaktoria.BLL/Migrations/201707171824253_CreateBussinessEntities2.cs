namespace eFaktoria.BLL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateBussinessEntities2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "NIP", c => c.String());
            AddColumn("dbo.InvoicePositions", "GrossValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }

        public override void Down()
        {
            DropColumn("dbo.InvoicePositions", "GrossValue");
            DropColumn("dbo.AspNetUsers", "NIP");
        }
    }
}
