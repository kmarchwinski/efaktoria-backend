namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateUserInvoiceMask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserInvoiceMasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Mask = c.String(),
                        InvoiceTypeId = c.Int(nullable: false),
                        StartNumber = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DictionaryPositions", t => t.InvoiceTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.InvoiceTypeId);
            
            AddColumn("dbo.Invoices", "ContractorName", c => c.String());
            AddColumn("dbo.Invoices", "ContractorAddress", c => c.String());
            AddColumn("dbo.Invoices", "ContractorPostalCode", c => c.String());
            AddColumn("dbo.Invoices", "ContractorNIP", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserInvoiceMasks", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserInvoiceMasks", "InvoiceTypeId", "dbo.DictionaryPositions");
            DropIndex("dbo.UserInvoiceMasks", new[] { "InvoiceTypeId" });
            DropIndex("dbo.UserInvoiceMasks", new[] { "UserId" });
            DropColumn("dbo.Invoices", "ContractorNIP");
            DropColumn("dbo.Invoices", "ContractorPostalCode");
            DropColumn("dbo.Invoices", "ContractorAddress");
            DropColumn("dbo.Invoices", "ContractorName");
            DropTable("dbo.UserInvoiceMasks");
        }
    }
}
