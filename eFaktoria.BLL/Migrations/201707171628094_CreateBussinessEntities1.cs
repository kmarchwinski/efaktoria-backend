namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateBussinessEntities1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountBanks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Number = c.Long(nullable: false),
                        IsMain = c.Boolean(nullable: false),
                        BankId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DictionaryPositions", t => t.BankId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.BankId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DictionaryPositions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DictionaryId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dictionaries", t => t.DictionaryId)
                .Index(t => t.DictionaryId);
            
            CreateTable(
                "dbo.Dictionaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        UserId = c.String(maxLength: 128),
                        City = c.String(),
                        AuthorizedPerson = c.String(),
                        AccountBankId = c.Int(nullable: false),
                        Remarks = c.String(),
                        CurrencyId = c.Int(nullable: false),
                        ContractorId = c.Int(nullable: false),
                        SellDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        IssueDate = c.DateTime(nullable: false),
                        PaymentMethodId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        InvoiceTypeId = c.Int(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountBanks", t => t.AccountBankId)
                .ForeignKey("dbo.Contractors", t => t.ContractorId)
                .ForeignKey("dbo.DictionaryPositions", t => t.CurrencyId)
                .ForeignKey("dbo.DictionaryPositions", t => t.InvoiceTypeId)
                .ForeignKey("dbo.DictionaryPositions", t => t.PaymentMethodId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.AccountBankId)
                .Index(t => t.CurrencyId)
                .Index(t => t.ContractorId)
                .Index(t => t.PaymentMethodId)
                .Index(t => t.InvoiceTypeId);
            
            CreateTable(
                "dbo.Contractors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                        NIP = c.Int(nullable: false),
                        Address = c.String(),
                        PostalCode = c.String(),
                        City = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        UserId = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.InvoicePositions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Count = c.Int(nullable: false),
                        Unit = c.String(),
                        VatRateId = c.Int(nullable: false),
                        NetPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InvoiceId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId)
                .ForeignKey("dbo.DictionaryPositions", t => t.VatRateId)
                .Index(t => t.VatRateId)
                .Index(t => t.InvoiceId);
            
            CreateTable(
                "dbo.InvoicePositionDictionaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Count = c.Int(nullable: false),
                        Unit = c.String(),
                        VatRateId = c.Int(nullable: false),
                        NetPrice = c.Decimal(precision: 18, scale: 2),
                        NetValue = c.Decimal(precision: 18, scale: 2),
                        UserId = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoicePositionDictionaries", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountBanks", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Invoices", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Invoices", "PaymentMethodId", "dbo.DictionaryPositions");
            DropForeignKey("dbo.Invoices", "InvoiceTypeId", "dbo.DictionaryPositions");
            DropForeignKey("dbo.InvoicePositions", "VatRateId", "dbo.DictionaryPositions");
            DropForeignKey("dbo.InvoicePositions", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "CurrencyId", "dbo.DictionaryPositions");
            DropForeignKey("dbo.Invoices", "ContractorId", "dbo.Contractors");
            DropForeignKey("dbo.Contractors", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Invoices", "AccountBankId", "dbo.AccountBanks");
            DropForeignKey("dbo.AccountBanks", "BankId", "dbo.DictionaryPositions");
            DropForeignKey("dbo.DictionaryPositions", "DictionaryId", "dbo.Dictionaries");
            DropIndex("dbo.InvoicePositionDictionaries", new[] { "UserId" });
            DropIndex("dbo.InvoicePositions", new[] { "InvoiceId" });
            DropIndex("dbo.InvoicePositions", new[] { "VatRateId" });
            DropIndex("dbo.Contractors", new[] { "UserId" });
            DropIndex("dbo.Invoices", new[] { "InvoiceTypeId" });
            DropIndex("dbo.Invoices", new[] { "PaymentMethodId" });
            DropIndex("dbo.Invoices", new[] { "ContractorId" });
            DropIndex("dbo.Invoices", new[] { "CurrencyId" });
            DropIndex("dbo.Invoices", new[] { "AccountBankId" });
            DropIndex("dbo.Invoices", new[] { "UserId" });
            DropIndex("dbo.DictionaryPositions", new[] { "DictionaryId" });
            DropIndex("dbo.AccountBanks", new[] { "UserId" });
            DropIndex("dbo.AccountBanks", new[] { "BankId" });
            DropTable("dbo.InvoicePositionDictionaries");
            DropTable("dbo.InvoicePositions");
            DropTable("dbo.Contractors");
            DropTable("dbo.Invoices");
            DropTable("dbo.Dictionaries");
            DropTable("dbo.DictionaryPositions");
            DropTable("dbo.AccountBanks");
        }
    }
}
