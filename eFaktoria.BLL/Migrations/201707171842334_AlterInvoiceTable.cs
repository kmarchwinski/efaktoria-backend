namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterInvoiceTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "GrossValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.InvoicePositions", "GrossValue");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InvoicePositions", "GrossValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Invoices", "GrossValue");
        }
    }
}
