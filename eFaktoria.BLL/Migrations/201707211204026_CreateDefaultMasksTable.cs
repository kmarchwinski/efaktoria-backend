namespace eFaktoria.BLL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateDefaultMasksTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DefaultInvoiceMasks",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Value = c.String(),
                    InvoiceTypeId = c.Int(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    CreatedBy = c.String(maxLength: 256),
                    UpdatedDate = c.DateTime(nullable: false),
                    UpdatedBy = c.String(maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DictionaryPositions", t => t.InvoiceTypeId)
                .Index(t => t.InvoiceTypeId);

            Sql(@"
INSERT INTO dbo.DefaultInvoiceMasks (Value,InvoiceTypeId,CreatedDate,UpdatedDate) VALUES ('%N/%M/%R','300','2017-07-17','2017-07-17');
INSERT INTO dbo.DefaultInvoiceMasks (Value,InvoiceTypeId,CreatedDate,UpdatedDate) VALUES ('%N/%M/%R/PRO','301','2017-07-17','2017-07-17');
INSERT INTO dbo.DefaultInvoiceMasks (Value,InvoiceTypeId,CreatedDate,UpdatedDate) VALUES ('%N/%M/%R/VM','302','2017-07-17','2017-07-17');

");

        }

        public override void Down()
        {
            DropForeignKey("dbo.DefaultInvoiceMasks", "InvoiceTypeId", "dbo.DictionaryPositions");
            DropIndex("dbo.DefaultInvoiceMasks", new[] { "InvoiceTypeId" });
            DropTable("dbo.DefaultInvoiceMasks");
        }
    }
}
