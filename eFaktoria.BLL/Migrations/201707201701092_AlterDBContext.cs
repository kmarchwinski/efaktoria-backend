namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterDBContext : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountBanks", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "Address", c => c.String());
            AddColumn("dbo.AspNetUsers", "PostalCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "City", c => c.String());
            DropColumn("dbo.AccountBanks", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountBanks", "Name", c => c.String());
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "PostalCode");
            DropColumn("dbo.AspNetUsers", "Address");
            DropColumn("dbo.AccountBanks", "IsActive");
        }
    }
}
