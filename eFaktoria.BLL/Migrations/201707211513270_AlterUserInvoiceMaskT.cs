namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserInvoiceMaskT : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserInvoiceMasks", "IsMain", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserInvoiceMasks", "Name", c => c.String());
            AddColumn("dbo.UserInvoiceMasks", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserInvoiceMasks", "IsActive");
            DropColumn("dbo.UserInvoiceMasks", "Name");
            DropColumn("dbo.UserInvoiceMasks", "IsMain");
        }
    }
}
