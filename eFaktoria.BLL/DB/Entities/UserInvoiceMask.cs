﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eFaktoria.BLL.DB.Entities
{
    public class UserInvoiceMask : AuditableEntity<int>
    {
        [Required]
        public string UserId { get; set; }

        public string Mask { get; set; }

        public int InvoiceTypeId { get; set; }

        public bool IsMain { get; set; }

        public string Name { get; set; }

        public int StartNumber { get; set; }

        public bool IsActive { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [ForeignKey("InvoiceTypeId")]
        public DictionaryPosition InvoiceType { get; set; }
    }
}
