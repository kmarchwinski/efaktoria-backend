﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eFaktoria.BLL.DB.Entities
{
    public class Invoice : AuditableEntity<int>
    {
        public string Title { get; set; }

        [Required]
        public string UserId { get; set; }

        public string City { get; set; }

        public string AuthorizedPerson { get; set; }

        public int AccountBankId { get; set; }

        public string Remarks { get; set; }

        public int CurrencyId { get; set; }

        public int ContractorId { get; set; }

        public string ContractorName { get; set; }

        public string ContractorAddress { get; set; }

        public string ContractorPostalCode { get; set; }

        public string ContractorNIP { get; set; }

        public decimal GrossValue { get; set; }

        /// <summary>
        /// Data sprzedaży
        /// </summary>
        public DateTime SellDate { get; set; }

        /// <summary>
        /// Data płatnośći
        /// </summary>
        public DateTime PaymentDate { get; set; }

        /// <summary>
        /// Data wystawienia
        /// </summary>
        public DateTime IssueDate { get; set; }

        public int PaymentMethodId { get; set; }

        public bool IsActive { get; set; }

        public int InvoiceTypeId { get; set; }

        public bool IsPaid { get; set; }

        [ForeignKey("InvoiceTypeId")]
        public virtual DictionaryPosition InvoiceType { get; set; }

        [ForeignKey("ContractorId")]
        public virtual Contractor Contractor { get; set; }

        [ForeignKey("AccountBankId")]
        public virtual AccountBank AccountBank { get; set; }

        [ForeignKey("CurrencyId")]
        public virtual DictionaryPosition Currency { get; set; }

        [ForeignKey("PaymentMethodId")]
        public virtual DictionaryPosition PaymentMethod { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<InvoicePosition> InvoicePositions { get; set; }
    }
}
