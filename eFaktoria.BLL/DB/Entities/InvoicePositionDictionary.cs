﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eFaktoria.BLL.DB.Entities
{
    public class InvoicePositionDictionary : AuditableEntity<int>
    {
        public string Name { get; set; }

        public int Count { get; set; }

        /// <summary>
        /// Jednostka
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// stawka vat
        /// </summary>
        public int VatRateId { get; set; }

        public decimal? NetPrice { get; set; }

        public decimal? NetValue { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }
}
