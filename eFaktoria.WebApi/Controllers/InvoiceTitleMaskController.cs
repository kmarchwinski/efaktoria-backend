﻿using eFaktoria.BLL.Services.Invoice;
using eFaktoria.BLL.Services.Masks;
using eFaktoria.Model.DTO.Masks;
using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{

    [Authorize]
    [RoutePrefix("backend/mask")]
    public class InvoiceTitleMaskController : _WebApiBaseController
    {
        private readonly IGenarateInvoiceTitleService _genarateInvoiceTitleService;
        private readonly IMasksService _masksService;
        public InvoiceTitleMaskController(IGenarateInvoiceTitleService genarateInvoiceTitleService, IMasksService masksService)
        {
            _genarateInvoiceTitleService = genarateInvoiceTitleService;
            _masksService = masksService;
        }


        /// <summary>
        /// Pobiera tytuł faktury poprzez podanie id maski usera
        /// </summary>
        /// <param name="id">Id maski użytkownika</param>
        /// <param name="defaultMaskId">Id domyślnej maski jeśli taka została wybrana</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTitleByMask")]
        public IHttpActionResult GetInvoiceTitleByMaskId(int? id, int? defaultMaskId)
        {
            var data = UserService(() => _genarateInvoiceTitleService.GetTitleByMaskId(id, defaultMaskId, UserId));

            return data;
        }

        /// <summary>
        /// Pobiera listę masek plus domyślną dla danego typu faktury
        /// </summary>
        /// <param name="invoiceTypeId">Typ faktury</param>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        public IHttpActionResult GetListOfMask(int invoiceTypeId)
        {
            var data = UserService(() => _masksService.GetMasksList(UserId, invoiceTypeId));

            return data;
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(CreateMaskDTO model)
        {
            var data = UserService(() => _masksService.CreateMask(UserId, model));

            return data;
        }

        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult Delete(int id)
        {
            var data = UserService(() => _masksService.DeleteMask(id, UserId));

            return data;
        }
    }
}
