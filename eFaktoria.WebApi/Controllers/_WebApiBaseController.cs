﻿using eFaktoria.Model;
using Microsoft.AspNet.Identity;
using System;
using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{
    public class _WebApiBaseController : ApiController
    {
        protected string UserId => User.Identity.GetUserId();

        protected IHttpActionResult UserService<T>(Func<ResultModel<T>> action)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ResultModel<T> result = action.Invoke();

            if (result == null)
            {
                return InternalServerError();
            }

            if (result.Status.Errors != null)
            {
                foreach (string error in result.Status.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            if (result.Status.AccessError)
            {
                return Unauthorized();
            }

            if (result.Status.CriticalError)
            {
                return InternalServerError();
            }

            if (result.Status.NotFoundItem)
            {
                return NotFound();
            }

            if (result.HasError)
            {
                return BadRequest(ModelState);
            }

            return Ok(result.Data);
        }
    }
}