﻿using eFaktoria.BLL.Services.Company;
using eFaktoria.Model.DTO.Company;
using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("backend/company")]
    public class MyCompanyController : _WebApiBaseController
    {
        private readonly ICompanyProfileService _companyProfileService;
        public MyCompanyController(ICompanyProfileService companyProfileService)
        {
            _companyProfileService = companyProfileService;
        }

        [HttpGet]
        [Route("data")]
        public IHttpActionResult GetData()
        {
            var data = UserService(() => _companyProfileService.GetCompanyData(UserId));

            return data;
        }


        [HttpPut]
        [Route("update")]
        public IHttpActionResult UpdateData(CompanyUpdateDTO dto)
        {
            var data = UserService(() => _companyProfileService.UpdateCompanyData(UserId, dto));

            return data;
        }
    }
}
