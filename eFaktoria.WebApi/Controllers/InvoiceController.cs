﻿using eFaktoria.BLL.Services.Invoice;
using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("backend/Invoice")]
    public class InvoiceController : _WebApiBaseController
    {
        private readonly IGenarateInvoiceTitleService _genarateInvoiceTitleService;
        private readonly IInvoiceListService _invoiceService;
        public InvoiceController(IInvoiceListService invoiceService, IGenarateInvoiceTitleService genarateInvoiceTitleService)
        {
            _invoiceService = invoiceService;
            _genarateInvoiceTitleService = genarateInvoiceTitleService;
        }

        [HttpGet]
        [Route("getCountInvoices")]
        public IHttpActionResult GetCountOfInvoice()
        {
            var data = UserService(() => _genarateInvoiceTitleService.GetCountOfInvoice(UserId));

            return data;
        }

        /// <summary>
        /// Pobiera tytuł faktury
        /// </summary>
        /// <param name="invoiceTypeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTitle")]
        public IHttpActionResult GetInvoiceTitle(int invoiceTypeId)
        {
            var data = UserService(() => _genarateInvoiceTitleService.GetTitle(UserId, invoiceTypeId));

            return data;
        }

        /// <summary>
        /// Pobiera listę faktur
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        public IHttpActionResult GetList(int pageNumber, int pageSize)
        {
            var list = UserService(() => _invoiceService.GetInvoiceListForUser(UserId, pageSize, pageNumber));

            return list;
        }

        /// <summary>
        /// Pobiera dane faktury
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("data")]
        public IHttpActionResult GetDetails(int id)
        {
            return Ok();
        }

        /// <summary>
        /// Potwierdzenie, że płatność została dokonana
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("confirm_payment")]
        public IHttpActionResult ConfirmPayment(int id)
        {
            return Ok();
        }

        /// <summary>
        /// Usuwa fakturę (de facto zmienia flagę IsActive na false
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok();
        }

        /// <summary>
        /// Dodaje nową fakturę do systemu
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        //ToDo: dopisać parametr i model do niego
        public IHttpActionResult Create()
        {
            return Ok();
        }

        /// <summary>
        /// Aktualizuje dane faktury
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("update")]
        //ToDo: dopisać parametr i model do niego
        public IHttpActionResult Update()
        {
            return Ok();
        }
    }
}
