﻿using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("backend/customer")]
    public class CustomerController : _WebApiBaseController
    {
        //private readonly IInvoiceService _invoiceService;
        //public CustomerController(IInvoiceService invoiceService)
        //{
        //    _invoiceService = invoiceService;
        //}

        /// <summary>
        /// Pobiera listę faktur
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        public IHttpActionResult GetList(int pageNumber, int pageSize)
        {
            //var list = UserService(() => _invoiceService.GetInvoiceListForUser(UserId, pageSize, pageNumber));

            return Ok();
        }

        [HttpGet]
        [Route("data")]
        public IHttpActionResult GetDetails(int id)
        {
            return Ok();
        }


        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult Delete(int id)
        {
            return Ok();
        }

        /// <summary>
        /// Dodaje nowego klienta
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        //ToDo: dopisać parametr i model do niego
        public IHttpActionResult Create()
        {
            return Ok();
        }

        /// <summary>
        /// Aktualizuje dane klienta
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("update")]
        //ToDo: dopisać parametr i model do niego
        public IHttpActionResult Update()
        {
            return Ok();
        }
    }
}
