﻿namespace eFaktoria.Core.Enums
{
    // ReSharper disable once InconsistentNaming
    public enum enuVATRate
    {
        Percent23 = 100,
        Percent8 = 101,
        Percent5 = 102,
        Percent0 = 103,
        TaxExempt = 104,
        // ReSharper disable once InconsistentNaming
        oo = 105
    }
}
