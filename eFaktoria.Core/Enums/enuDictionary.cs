﻿namespace eFaktoria.Model.Enums
{
    // ReSharper disable once InconsistentNaming
    public enum enuDictionary
    {
        VATRate = 1,
        InvoiceTitleSymbols = 5
    }
}
