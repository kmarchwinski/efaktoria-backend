﻿using System.Collections.Generic;
using System.Linq;

namespace eFaktoria.Core.Helpers
{
    public static class PagingHelper
    {
        public static IEnumerable<T> Page<T>(this IEnumerable<T> en, int pageSize, int page)
        {
            int skip = page - 1;
            return en.Skip(skip * pageSize).Take(pageSize);
        }

        public static IQueryable<T> Page<T>(this IQueryable<T> en, int pageSize, int page)
        {
            int skip = page - 1;
            return en.Skip(skip * pageSize).Take(pageSize);
        }
    }
}